import { shallow } from "enzyme";
import React from "react";

import { Loader } from "../../../src/components/Loader";

it("renders without crashing", () => {
  expect(() => shallow(<Loader phrase="whatever" />)).not.toThrowError();
});
